var express = require('express');
var router = express.Router();

router.get('/read', (req, res) => {
    let body = {'name': 'test event', 'description': 'Test event for demo only'};
    return res.status(200).send(body)
})

router.get('/read/:evid', (req, res) => {
    let body = {'id': parseInt(req.params.evid),'name': 'test event', 'description': 'Test event for demo only'};
    return res.status(200).send(body)
})

router.post('/create', (req, res) => {
    return res.status(200).send("dummy")
})

router.post('/update/:evid', (req, res) => {
    return res.status(200).send("dummy")
})

router.get('/delete/:evid', (req, res) => {
    return res.status(200).send("dummy")
})

module.exports = router;
