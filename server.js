var yaml = require('yamljs');
var config = yaml.load('.env')
let express = require('express');
var bodyParser = require('body-parser');
var routes = require('./routes/index')
var http = require('http');
let app = express()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.get('/', (req, res) => {
    let body = "Event service";
    return res.status(200).send(body)
})
app.use(config.middleurl , routes);

app.set('port', config.port || 3000); //atau bisa pakai process.env.PORT
var server = http.createServer(app);
server.listen(app.get('port'), function(){
    console.log('[NODE] Service listening on port', app.get('port'))
})
